#Targeting system = Motoko uses objects to keep track of target profiles. You can designate a target profile and save it.
#You can load a saved target profile for attacking or automating surveillance of a previously discovered target.
#A target_profile contains the following data:
#Unique ID
#Operator Name (name used to remember device or target)
#Type: AP(s), Node(s), Channel(s)
#Channel(s) observed operating on
#MAC address
#Alias (randomized) MAC addresses used when not unassociated
#AP's connected to (data connections observed)
#Handshake captures for associated network: (format: AP_ESSID, cap_file, target_profile)

#ESP8266 Modules -
#Karma Decloak nearby devices
#Passive listen to nearby networks
#Beacon storm device decloaking
# Track target - Via list, Or add target
#Device data hijacking
#Reverse WPA Attacks
#Find Organization Members by SSID

#Mo - Mobile Operations
#1 - See nearby AP's, Nodes, and Data connections - Select channel filters
#2 - Designate surveillance targets - Select channel, AP, or Node - Set duration of surveillance
#3 - Scan for known targets - Load a target profile or add a target - Set SSID of network to track devices sending probe requests to matching SSID
#4 - Distinguish mobile nodes from background noise - Gather data and exclude nodes with inconsistent RSSI
#5 - Track unassociated devices - Track nodes which are not connected to a mobile network

#To - Tracking Operations
#1 - Unmask Nodes - Select Reactive list of fake SSID's from probe requests - Select top 500 most common SSID's - Save target profile with real & random MAC, networks responded to
#2 - Capture Nodes - Attempt capture with list of open AP's - Attempt half-handshake online attack


#Ko - Kill Operations

#Operations to DETECT, CLASSIFY, CAPTURE, INTERCEPT, DECEIVE, JAM and INJECT into wireless networks from captured (internal) and external network positions
#1 - Detect active data links and provide target list with MAC address mfg, channel, signal strength, encryption
#2 - Blind all data links containing a MAC address known to be an Wi-Fi camera
#3 - Monitor target data throughput
#4 - Jam Nodes (Traffic to and from a client device or devices)
#5 - Jam Channels (Jam an entire channel)
#6 - Jam AP's (Jam only communications from one AP)
#7 - Attempt capture and freeze connection internally CARP-poisoning)
#8 - Broadcast deceptive AP's - (Attempt debuth and flood area with fake AP's, emulating ESS)

#9 - There is no section 9

#(On entering password)

#SECTION 9 - Electronic Warfare Control

#A - Load target profile
#B - Manage wireless interface
#C - Jamming Operations
#C1 - Constant jammer - Set channel - Go
#C2 - Deceptive jammer - Select channel - Go
#C3 - Random Jammer - Set Packets, Bits, Both - Set random interval or formula - Set channel - Go
#C4 - Reactive Jammer - Set channel target - Set RTS/CTS or Data/ACK - Go
#D - Advanced Jamming Operations
#D1 - Follow on jammer - Select channels to jam
#D2 - Scan jammer - Set target profile(s) to scan continuously all channels for - Scan continuously all channels for and jam all connected nodes 
